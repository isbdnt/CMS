/**
 * Created by isbdnt on 2017/12/13.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { renderWithLanguage, renderWithoutUser, } from 'container-wrappers';

class SignUpPage extends Component {
    render() {
        return h('div', 'sign up page');
    }
}
export default renderWithLanguage(renderWithoutUser(SignUpPage));