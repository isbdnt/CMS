/**
 * Created by isbdnt on 2017/12/13.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { renderWithLanguage, renderWithoutUser, } from 'container-wrappers';

class SignInPage extends Component {
    render() {
        return h('div', 'sign in page');
    }
}
export default renderWithLanguage(renderWithoutUser(SignInPage));