/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { ItemList, NavBar, } from 'containers';
import { renderWithLanguage, } from 'container-wrappers';

class SearchPage extends Component {
    render() {
        return h('div', {
            children: [
                h(NavBar, { key: 0, }),
                h(ItemList, { key: 1, }),
            ],
        });
    }
}
export default renderWithLanguage(SearchPage);