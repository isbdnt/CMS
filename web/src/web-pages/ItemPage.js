/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { Item, NavBar, } from 'containers';
import { renderWithLanguage, } from 'container-wrappers';

class ItemPage extends Component {
    render() {
        return h('div', {
            children: [
                h(NavBar, { key: 0, }),
                h(Item, { key: 1, }),
            ],
        });
    }
}
export default renderWithLanguage(ItemPage);