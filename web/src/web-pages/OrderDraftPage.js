/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { OrderDraft, } from 'containers';
import { renderWithLanguage, renderWithUser, } from 'container-wrappers';

class OrderDraftPage extends Component {
    render() {
        return h('div', {
            children: h(OrderDraft),
        });
    }
}
export default renderWithLanguage(renderWithUser(OrderDraftPage));