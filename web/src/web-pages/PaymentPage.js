/**
 * Created by isbdnt on 2017/12/27.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { Payment, } from 'containers';
import { renderWithLanguage, renderWithUser, } from 'container-wrappers';

class PaymentPage extends Component {
    render() {
        return h('div', {
            children: h(Payment),
        });
    }
}
export default renderWithLanguage(renderWithUser(PaymentPage));