/**
 * Created by isbdnt on 2017/12/13.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { NavBar, AdvisedSearch, NavColumn, SubjectMarket, UserProfile, } from 'containers';
import logo from 'resources/images/logo.png';
import { Container, Row, Col, Hidden, } from 'react-grid-system';
import PropTypes from 'prop-types';
import { renderWithLanguage, } from 'container-wrappers';

class IndexPage extends Component {
    static childContextTypes = {
        gutterWidth: PropTypes.number,
    };
    getChildContext = () => ({
        gutterWidth: 8,
    });

    render() {
        return h('div', {
            children: [
                h(NavBar, { key: 0, path: '/', }),
                h('div', {
                    key: 1,
                    children: [
                        h('img', { key: 0, src: logo, }),
                        h(AdvisedSearch, { key: 1, }),
                    ],
                }),
                h(NavColumn, {
                    key: 2,
                }),
                h(Container, {
                    key: 3,
                    children: h(Row, {
                        children: [
                            h(Col, {
                                key: 0,
                                md: 8,
                                style: {},
                                children: [
                                    h(Row, {
                                        key: 0,
                                        children: [
                                            h(Hidden, {
                                                key: 0,
                                                xs: true,
                                                children: h(Col, {
                                                    sm: 4,
                                                    children: h(SubjectMarket),
                                                }),
                                            }),
                                            h(Col, {
                                                key: 1,
                                                sm: 8,
                                                style: {},
                                                children: [
                                                    h(Row, {
                                                        key: 0,
                                                        children: h(Col, {
                                                            style: {
                                                                backgroundColor: 'pink',
                                                                backgroundClip: 'content-box',
                                                            },
                                                            children: 'One of three columns',
                                                        }),
                                                    }),
                                                    h(Row, {
                                                        key: 1,
                                                        children: h(Col, {
                                                            style: {
                                                                backgroundColor: 'pink',
                                                                backgroundClip: 'content-box',
                                                                marginTop: 8,
                                                            },
                                                            children: 'One of three columns',
                                                        }),
                                                    }),
                                                ],
                                            }),
                                        ],
                                    }),
                                    h(Row, {
                                        key: 1,
                                        children: h(Col, {
                                            xs: 12,
                                            style: {
                                                backgroundColor: 'pink',
                                                backgroundClip: 'content-box',
                                                marginTop: 8,
                                            },
                                            children: 'One of three columns',
                                        }),
                                    }),
                                ],
                            }),
                            h(Hidden, {
                                xs: true,
                                sm: true,
                                key: 1,
                                children: h(Col, {
                                    md: 4,
                                    style: {
                                        backgroundColor: 'red',
                                        backgroundClip: 'content-box',
                                    },
                                    children: h(UserProfile),
                                }),
                            }),
                        ],
                    }),
                }),
            ],
        });
    }
}
export default renderWithLanguage(IndexPage);