/**
 * Created by isbdnt on 2017/12/13.
 */
import IndexPage from './IndexPage';
import SignInPage from './SignInPage';
import SignUpPage from './SignUpPage';
import UserPage from './UserPage';
import OrderPage from './OrderPage';
import SearchPage from './SearchPage';
import ItemPage from './ItemPage';
import DraftOrderPage from './OrderDraftPage';
import ShoppingCartPage from './ShoppingCartPage';
import PaymentPage from './PaymentPage';
export {
    IndexPage,
    SignInPage,
    SignUpPage,
    UserPage,
    OrderPage,
    SearchPage,
    ItemPage,
    DraftOrderPage,
    ShoppingCartPage,
    PaymentPage,
};