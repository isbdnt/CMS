/**
 * Created by isbdnt on 2017/12/13.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { NavBar, TradeSideBar, BoughtItems, } from 'containers';
import { Container, Row, Col, } from 'react-grid-system';
import { Route, Router, } from 'react-router-dom';
import internalSites from 'resources/configs/internalSites.json';
import { renderWithUser, renderWithLanguage, } from 'container-wrappers';
import history from 'browser-history';

class UserPage extends Component {
    render() {
        return h(Router, {
            history,
            children: h('div', {
                children: [
                    h(NavBar, { key: 0, }),
                    h(Container, {
                        key: 1,
                        children: h(Row, {
                            children: [
                                h(Col, {
                                    key: 0,
                                    sm: 4,
                                    children: h(TradeSideBar, {}),
                                }),
                                h(Col, {
                                    key: 1,
                                    sm: 8,
                                    children: [
                                        h(Route, {
                                            key: 0,
                                            path: internalSites.user.boughtItems.$url,
                                            render: () => h(BoughtItems),
                                        }),
                                        h(Route, {
                                            key: 1,
                                            path: internalSites.user.purchasedShop.$url,
                                            render: () => h('div', { children: 'purchasedShop', }),
                                        }),
                                    ],
                                }),
                            ],
                        }),
                    }),
                ],
            }),
        });
    }
}
export default renderWithLanguage(renderWithUser(UserPage));