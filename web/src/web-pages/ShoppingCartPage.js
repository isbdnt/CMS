/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { ShoppingCart, NavBar, } from 'containers';
import { renderWithLanguage, renderWithUser, } from 'container-wrappers';

class ShoppingCartPage extends Component {
    render() {
        return h('div', {
            children: [
                h(NavBar, { key: 0, }),
                h(ShoppingCart, { key: 1, }),
            ],
        });
    }
}
export default renderWithLanguage(renderWithUser(ShoppingCartPage));