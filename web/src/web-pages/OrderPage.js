/**
 * Created by Administrator on 2017.12.15.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { Order, } from 'containers';
import { renderWithLanguage, renderWithUser, } from 'container-wrappers';

class OrderPage extends Component {
    render() {
        return h('div', {
            children: h(Order),
        });
    }
}
export default renderWithLanguage(renderWithUser(OrderPage));