/**
 * Created by isbdnt on 2017/12/24.
 */
import items from 'resources/mock/items.json';
import { Observable, } from 'rxjs';

export default ({ itemID, }) => {
    console.log(`get:/items/${itemID}`);
    return Observable.of({
        data: items.find(item => item.id === itemID),
    });
};