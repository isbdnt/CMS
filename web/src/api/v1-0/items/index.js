/**
 * Created by isbdnt on 2017/12/24.
 */
import get from './get';
import getMany from './getMany';

export default {
    get, getMany,
};