/**
 * Created by isbdnt on 2017/12/24.
 */
import items from 'resources/mock/items.json';
import { Observable, } from 'rxjs';

export default ({ keywords, offset, count, }) => {
    console.log(`getMany:/items`);
    return Observable.of({
        data: items.filter(item => keywords.findIndex(keyword => item.name.includes(keyword)) >= 0)
            .slice(offset, count),
    });
};