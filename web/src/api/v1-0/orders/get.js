/**
 * Created by isbdnt on 2017/12/16.
 */
import orders from 'resources/mock/orders.json';
import { Observable, } from 'rxjs';

export default ({ orderID, }) => {
    console.log(`get:/orders/${orderID}`);
    return Observable.of({
        data: orders.find(order => order.id === orderID),
    });
};