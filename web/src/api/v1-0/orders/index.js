/**
 * Created by isbdnt on 2017/12/24.
 */
import get from './get';
import patch from './patch';

export default {
    get, patch,
};