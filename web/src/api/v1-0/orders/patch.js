/**
 * Created by isbdnt on 2017/12/26.
 */
import { Observable, } from 'rxjs';
import orders from 'resources/mock/orders.json';
import users from 'resources/mock/users.json';

export default ({ orderID, type, ...rest }) => {
    console.log(`patch:/orders/${orderID}`);
    const index = orders.findIndex(order => order.id === orderID);
    const order = orders[ index ];
    switch (type) {
        case 'PAY_AMOUNT':
            if (order.customer.paymentPassword === rest.paymentPassword) {
                const user = users.find(user => user.id === order.customer.id);
                user.balance -= order.amount;
                order.state = 'pendingDelivery';
                user.pendingPaymentCount--;
                user.pendingDeliveryCount++;
            }
    }
    return Observable.of({
        data: order,
    });
};