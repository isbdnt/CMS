/**
 * Created by isbdnt on 2017/12/25.
 */
import getMany from './getMany';

export default {
    getMany,
};