/**
 * Created by isbdnt on 2017/12/25.
 */
import { Observable, } from 'rxjs';
import searchAdvices from 'resources/mock/search-advices.json';

export default ({ keywords, }) => {
    console.log(`getMany:/search_advices`);
    return Observable.of({
        data: searchAdvices.filter(advice => keywords.findIndex(word => advice.includes(word)) >= 0),
    });
};