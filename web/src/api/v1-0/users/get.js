/**
 * Created by isbdnt on 2017/12/24.
 */
import { Observable, } from 'rxjs';
import users from 'resources/mock/users.json';

//todo session/token
const token = 'sad';
export default () => {
    console.log(`get:/users/${users[ 0 ].id}`);
    return Observable.of({
        data: users[ 0 ],
        // data: null,
    });
};