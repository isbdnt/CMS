/**
 * Created by isbdnt on 2017/12/25.
 */
import post from './post';
import patch from './patch';
import remove from './remove';
export default {
    post, patch, remove,
};