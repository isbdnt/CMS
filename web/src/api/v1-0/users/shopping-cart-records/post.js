/**
 * Created by isbdnt on 2017/12/25.
 */
import { Observable, } from 'rxjs';
import records from 'resources/mock/shopping-cart-records.json';
import items from 'resources/mock/items.json';

let id = 1;

export default ({ userID, itemID, count, }) => {
    console.log(`post:/users/${userID}/shopping_cart_records`);
    const record = {
        id: id++,
        item: items.find(item => item.id === itemID),
        count,
    };
    records.push(record);
    return Observable.of({
        data: record,
    });
};