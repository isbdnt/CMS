/**
 * Created by isbdnt on 2017/12/25.
 */
import { Observable, } from 'rxjs';
import records from 'resources/mock/shopping-cart-records.json';

export default ({ userID, recordID, type, ...rest }) => {
    console.log(`patch:/users/${userID}/shopping_cart_records/${recordID}`);
    const index = records.findIndex(record => record.id === recordID);
    switch (type) {
        case 'UPDATE_COUNT':
            records[ index ].count = rest.count;
    }
    return Observable.of({
        data: records[ index ],
    });
};