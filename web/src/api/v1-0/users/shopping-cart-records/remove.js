/**
 * Created by isbdnt on 2017/12/25.
 */
import { Observable, } from 'rxjs';
import records from 'resources/mock/shopping-cart-records.json';

export default ({ userID, recordID, }) => {
    console.log(`remove:/users/${userID}/shopping_cart_records/${recordID}`);
    records.splice(records.findIndex(record => record.id === recordID), 1);
    return Observable.of({
        data: {},
    });
};