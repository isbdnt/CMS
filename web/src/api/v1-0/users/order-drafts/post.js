/**
 * Created by isbdnt on 2017/12/26.
 */
import drafts from 'resources/mock/order-drafts.json';
import users from 'resources/mock/users.json';
import items from 'resources/mock/items.json';
import addresses from 'resources/mock/addresses.json';
import { Observable, } from 'rxjs';

let id = 1;
let recordID = 1;

export default ({ userID, shoppingCartRecords, }) => {
    console.log(`post:/users/${userID}/order_drafts`);
    const customer = users.find(user => user.id === userID);
    const draft = {
        id: id++,
        createdAt: +new Date(),
        purchaseRecords: shoppingCartRecords.map(({ itemID, count, }) => ({
            id: recordID++,
            item: items.find(item => item.id === itemID),
            count,
            amount: 49.99,
        })),
        address: addresses.find(address => address.id === customer.defaultAddress.id),
        customer,
        currency: 'cny',
        amount: 249.99,
    };
    drafts.push(draft);
    return Observable.of({
        data: draft,
    });
};