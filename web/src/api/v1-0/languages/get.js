/**
 * Created by isbdnt on 2017/12/25.
 */
import langEN from 'resources/mock/languages/en.json';
import langZHCN from 'resources/mock/languages/zh-CN.json';
import { Observable, } from 'rxjs';

export default ({ region, }) => {
    let lang = {};
    switch (region) {
        case 'global':
        case 'us':
            lang = langEN;
            break;
        case 'cn':
        default:
            lang = langZHCN;
            break;
    }
    console.log(`get:/languages/${region}`);
    return Observable.of({
        data: lang,
    });
};