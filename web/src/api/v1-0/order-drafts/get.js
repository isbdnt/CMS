/**
 * Created by isbdnt on 2017/12/16.
 */
import drafts from 'resources/mock/order-drafts.json';
import { Observable, } from 'rxjs';

export default ({ draftID, }) => {
    console.log(`get:/order_drafts/${draftID}`);
    return Observable.of({
        data: drafts.find(draft => draft.id === draftID),
    });
};