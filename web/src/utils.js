/**
 * Created by isbdnt on 2017/12/13.
 */
import wordsRegexes from 'resources/configs/words-regexes.json';
import _ from 'lodash';
import numericalValues from 'resources/configs/numerical-values.json';
import { Observable, } from 'rxjs';
import qs from 'qs';

export const splitWords = (str, languageType) => {
    if (str)
        return str.match(new RegExp(wordsRegexes[ languageType ], "g"));
    return [];
};
export const formatDate = (timestamp, format) => {
    if (!timestamp)return '';
    const date = new Date(timestamp);
    const o = {
        "M+": date.getMonth() + 1,                 //月份
        "d+": date.getDate(),                    //日
        "h+": date.getHours(),                   //小时
        "m+": date.getMinutes(),                 //分
        "s+": date.getSeconds(),                 //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds(),             //毫秒
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (const k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[ k ]) : (("00" + o[ k ]).substr(("" + o[ k ]).length)));
        }
    }
    return format;
};
export const createAmountLable = (amount, currency, language) => {
    if (_.isNumber(amount) === false)return '';
    return language.$type === 'zh-CN' ? amount.toFixed(2) + language[ currency ] : language[ currency ] + amount.toFixed(2);
};
// export const throttleValues = (obj) => _.mapValues(obj, (val) => {
//     if (_.isFunction(val)) {
//         let ob$ = null;
//         const throttled = _.throttle(val, numericalValues.throttleWait);
//         return (...args) => {
//             const newOb$ = throttled(...args);
//             if (ob$ === newOb$) {
//                 return Observable.empty();
//             }
//             ob$ = newOb$;
//             return newOb$;
//         };
//     }
//     else {
//         return val;
//     }
// });

export const getWindowQuery = () => {
    const queryStr = window.location.href.match(/\?(.*)/);
    if (queryStr) {
        return qs.parse(queryStr[ 1 ]);
    }
    return {};
};

export const getIntervalObservable = () => Observable.interval(numericalValues.throttleInterval);

export const requireDocument = (document, id, cb) => {
    id = parseInt(id);
    if (!document || document.id !== id) {
        cb(id);
    }
};