/**
 * Created by isbdnt on 2017/12/13.
 */
import { Component, } from 'react';
import { Container, Row, Col, } from 'react-grid-system';
import h from 'react-hyperscript';
import externalSites from 'resources/configs/externalSites.json';
import { connect, } from 'react-redux';

class NavColumn extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { language, } = this.props;
        return h(Container, {
            children: h(Row, {
                children: h(Col, {
                    children: [
                        { label: language.tmall, href: externalSites.tmall, },
                        { label: language.ju, href: externalSites.ju, },
                        { label: language.tmallSupermarket, href: externalSites.tmallSupermarket, },
                        { label: language.panicBuying, href: externalSites.panicBuying, },
                    ].map(({ label, href, }, key) =>
                        h('span', {
                            key,
                            children: h('a', {
                                href, children: label,
                            }),
                        })),
                }),
            }),
        });
    }
}

const mapStateToProps = ({ language, }) => ({
    language,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NavColumn);
