/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import internalSites from 'resources/configs/internalSites.json';
import { connect, } from 'react-redux';
import QueryLink from './QueryLink';
import { createAmountLable, } from 'utils';
import { createOrderDraft, } from 'sources/order-draft/actions';
import { replaceItem, } from 'sources/item/actions';

class ShoppingCart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            checks: [],
            checkCount: 0,
        };
    }

    render() {
        const {
            language, shoppingCartRecords, onClickItemName, currency, onCheckout,
        } = this.props;
        return h('div', {
            style: {
                display: 'flex',
                flexDirection: 'column',
            },
            children: [
                h('div', {
                    key: 0,
                    children: [
                        h('span', {
                            key: 0,
                            children: [
                                h('input', {
                                    key: 0,
                                    type: 'checkbox',
                                    checked: this.state.checkCount === shoppingCartRecords.length,
                                    onChange: (e) => {
                                        const checks = Array.from(this.state.checks);
                                        for (let i = 0; i < shoppingCartRecords.length; i++) {
                                            checks[ i ] = e.target.checked;
                                        }
                                        const checkCount = e.target.checked ? shoppingCartRecords.length : 0;
                                        this.setState({ checks, checkCount, });
                                    },
                                }),
                                h('span', {
                                    key: 1,
                                    children: '全选',
                                }),
                            ],
                        }),
                    ],
                }),
                h('div', {
                    key: 1,
                    children: shoppingCartRecords.map(({ item, count, }, key) =>
                        h('div', {
                            key,
                            children: [
                                h('input', {
                                    key: -1,
                                    type: 'checkbox',
                                    checked: this.state.checks[ key ] || false,
                                    onChange: (e) => {
                                        const checks = Array.from(this.state.checks);
                                        checks[ key ] = e.target.checked;
                                        this.setState({
                                            checks,
                                            checkCount: this.state.checkCount + (e.target.checked ? 1 : -1),
                                        });
                                    },
                                }),
                                h('img', {
                                    key: 0,
                                    src: item.img,
                                }),
                                h(QueryLink, {
                                    key: 1,
                                    to: internalSites.item.$url,
                                    query: {
                                        itemID: item.id,
                                    },
                                    onClick: () => onClickItemName && onClickItemName(item),
                                    children: item.name,
                                }),
                                h('span', {
                                    key: 2,
                                    children: createAmountLable(item.price, currency, language),
                                }),
                                h('span', {
                                    key: 3,
                                    children: count,
                                }),
                                h('span', {
                                    key: 4,
                                    children: createAmountLable((item.price * count).toFixed(2), currency, language),
                                }),
                            ],
                        })),
                }),
                h('div', {
                    key: 2,
                    onClick: () => {
                        const records = shoppingCartRecords
                            .filter((record, key) => this.state.checks[ key ]);
                        if (records.length) {
                            onCheckout && onCheckout(records);
                        }
                    },
                    children: '结算',
                }),
            ],
        });
    }
}

const mapStateToProps = ({ language, shoppingCartRecords, currency, }) => ({
    language,
    shoppingCartRecords,
    currency,
});

const mapDispatchToProps = dispatch => ({
    onCheckout: (shoppingCartRecords) => dispatch(createOrderDraft(shoppingCartRecords)),
    onClickItemName: (item) => dispatch(replaceItem(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);