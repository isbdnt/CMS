/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import internalSites from 'resources/configs/internalSites.json';
import { connect, } from 'react-redux';
import { fetchItems, } from 'sources/items/actions';
import QueryLink from './QueryLink';
import { replaceItem, } from 'sources/item/actions';

class ItemList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 0,
        };
    }

    render() {
        const { language, items, query, onRequireItems, onClickItemName, } = this.props;
        const itemsOfPage = (items[ query.keyword ] || {})[ this.state.page ];
        if (!itemsOfPage)
            onRequireItems && onRequireItems(query.keyword, this.state.page);
        return h('div', {
            style: {
                display: 'flex',
                flexDirection: 'column',
            },
            children: itemsOfPage ? itemsOfPage.map((item, key) =>
                h('div', {
                    key,
                    children: [
                        h('img', { key: 0, src: item.img, }),
                        h(QueryLink, {
                            key: 1,
                            to: internalSites.item.$url,
                            query: {
                                itemID: item.id,
                            },
                            onClick: () => onClickItemName && onClickItemName(item),
                            children: item.name,
                        }),
                        h('span', {
                            key: 2,
                            children: item.price,
                        }),
                    ],
                })) : null,
        });
    }
}

const mapStateToProps = ({ language, items, query, }) => ({
    language,
    items,
    query,
});

const mapDispatchToProps = dispatch => ({
        onRequireItems: (keyword, page) =>
            setImmediate(() => dispatch(fetchItems(keyword, page))),
        onClickItemName: (item) => dispatch(replaceItem(item)),
    })
;

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);