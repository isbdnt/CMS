/**
 * Created by isbdnt on 2017/12/13.
 */
import { Component, } from 'react';
import { Container, Row, Col, } from 'react-grid-system';
import h from 'react-hyperscript';
import internalSites from 'resources/configs/internalSites.json';
import { connect, } from 'react-redux';
import QueryLink from './QueryLink';

class UserProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { language, user, } = this.props;
        return h(Container, {
            children: h(Row, {
                children: h(Col, {
                    children: user ? [
                        h('div', {
                            key: 0,
                            children: [
                                h('img', { key: 0, }),
                                h('div', { key: 1, children: user.nickname, }),
                            ],
                        }),
                        h('div', {
                            key: 1,
                            children: [
                                {
                                    label: language.pendingConfirming,
                                    to: internalSites.user.boughtItems.$url,
                                    count: user.pendingReceivingCount,
                                    query: { collectionType: 'pendingConfirming', },
                                },
                                {
                                    label: language.pendingDelivery,
                                    to: internalSites.user.boughtItems.$url,
                                    count: user.pendingDeliveryCount,
                                    query: { collectionType: 'pendingDelivery', },
                                },
                                {
                                    label: language.pendingPayment,
                                    to: internalSites.user.boughtItems.$url,
                                    count: user.pendingPaymentCount,
                                    query: { collectionType: 'pendingPayment', },
                                },
                                {
                                    label: language.pendingEvaluating,
                                    to: internalSites.user.boughtItems.$url,
                                    count: user.pendingEvaluatingCount,
                                    query: { collectionType: 'pendingEvaluating', },
                                },
                            ].map(({ label, to, count, query, }, key) =>
                                h('span', {
                                    key,
                                    children: [
                                        h('span', {
                                            key: 0,
                                            children: count,
                                        }),
                                        h(QueryLink, {
                                            key: 1,
                                            to,
                                            query,
                                            children: label,
                                        }),
                                    ],
                                })),
                        }),
                    ] : null,
                }),
            }),
        });
    }
}

const mapStateToProps = ({ language, user, }) => ({
    language,
    user,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);