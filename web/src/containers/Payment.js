/**
 * Created by isbdnt on 2017/12/27.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import { fetchPayment, userPayAmount, } from 'sources/payment/actions';

class Payment extends Component {
    constructor(props) {
        super(props);
        const { query, paymentAmount, onRequirePaymentAmount, } = this.props;
        if (!paymentAmount) {
            const orderIDs = query.orderID ?
                [ parseInt(query.orderID), ] : query.orderIDs.map(id => parseInt(id));
            onRequirePaymentAmount && onRequirePaymentAmount(orderIDs);
            this.orderIDs = orderIDs;
        }
    }

    render() {
        const {
            language, payment, onConfirmPaymentAmount
        } = this.props;
        if (!payment)return null;

        return h('div', {
            children: [
                h('div', {
                    key: 0,
                    children: payment.amount,
                }),
                h('input', {
                    key: 1,
                    type: 'password',
                    ref: 'password',
                }),
                h('div', {
                    key: 2,
                    onClick: () =>
                    payment.paid < payment.amount && onConfirmPaymentAmount && onConfirmPaymentAmount(this.refs.password.value, this.orderIDs),
                    children: payment.paid === payment.amount ? '付款完成' : '确认付款',
                }),
            ],
        });
    }
}

const mapStateToProps = ({ language, payment, query, currency, }) => ({
    language,
    payment,
    query,
    currency,
});

const mapDispatchToProps = dispatch => ({
    onRequirePaymentAmount: (orderIDs) => dispatch(fetchPayment(orderIDs)),
    onConfirmPaymentAmount: (paymentPassword, orderIDs) => dispatch(userPayAmount(paymentPassword, orderIDs)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Payment);