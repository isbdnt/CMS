/**
 * Created by isbdnt on 2017/12/13.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import internalSites from 'resources/configs/internalSites.json';
import { connect, } from 'react-redux';
import { Link, } from 'react-router-dom';

class TradeSideBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { language, } = this.props;
        return h('div', {
            style: {
                display: 'flex',
                flexDirection: 'column',
            },
            children: [
                { label: language.shoppingCartRecords, to: internalSites.shoppingCart.$url, },
                { label: language.boughtItems, to: internalSites.user.boughtItems.$url, },
                { label: language.purchasedShop, to: internalSites.user.purchasedShop.$url, },
                { label: language.collectedItems, to: internalSites.user.collectedItems.$url, },
            ].map(({ label, to, }, key) =>
                h('div', {
                    key,
                    children: h(Link, {
                        to, children: label,
                    }),
                })),
        });
    }
}

const mapStateToProps = ({ language, }) => ({
    language,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TradeSideBar);