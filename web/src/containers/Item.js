/**
 * Created by isbdnt on 2017/12/24.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import { fetchItem, } from 'sources/item/actions';
import { addItemToShoppingCart, } from 'sources/shopping-cart-records/actions';
import { createOrderDraft, } from 'sources/order-draft/actions';
import { requireDocument, } from 'utils';
class Item extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 1,
        };
        const { query, item, onRequireItem, } = props;
        requireDocument(item, query.itemID, onRequireItem);
    }

    render() {
        const {
            language, item, onAddItemToShoppingCart, onPurchase,
        } = this.props;
        if (!item)return null;
        return item ? h('div', {
            style: {
                display: 'flex',
                flexDirection: 'row',
            },
            children: [
                h('img', { key: 0, src: item.img, }),
                h('div', {
                    key: 1,
                    children: [
                        h('div', {
                            key: 0,
                            children: item.name,
                        }),
                        h('div', {
                            key: 1,
                            children: item.price,
                        }),
                        h('div', {
                            key: 2,
                            children: [
                                h('span', {
                                    key: 0,
                                    onClick: () =>
                                        this.setState({
                                            count: this.state.count > 1 ? this.state.count - 1 : 1,
                                        }),
                                    children: '-',
                                }),
                                h('input', {
                                    key: 1,
                                    onChange: (e) => {
                                        let count = 0;
                                        if (e.target.value === "") {
                                            count = "";
                                        }
                                        else {
                                            count = parseInt(e.target.value);
                                            count = count > 0 ? count : 1;
                                        }
                                        this.setState({ count, });
                                    },
                                    value: this.state.count,
                                }),
                                h('span', {
                                    key: 2,
                                    onClick: () => this.setState({ count: this.state.count + 1, }),
                                    children: '+',
                                }),
                            ],
                        }),
                        h('div', {
                            key: 3,
                            onClick: () => onPurchase(item, this.state.count),
                            children: '立即购买',
                        }),
                        h('span', {
                            key: 4,
                            onClick: () => onAddItemToShoppingCart(item, this.state.count),
                            children: '加入购物车',
                        }),
                    ],
                }),
                h('div', {
                    key: 2,
                }),
            ],
        }) : null;
    }
}

const mapStateToProps = ({ language, item, query, shoppingCartRecords, }) => ({
    language,
    item,
    query,
    shoppingCartRecords,
});

const mapDispatchToProps = dispatch => ({
    onRequireItem: (itemID) => setImmediate(() => dispatch(fetchItem(itemID))),
    onAddItemToShoppingCart: (item, count) => dispatch(addItemToShoppingCart(item, count)),
    onPurchase: (item, count) => dispatch(createOrderDraft([ { item, count, }, ])),
});

export default connect(mapStateToProps, mapDispatchToProps)(Item);