/**
 * Created by Administrator on 2017.12.15.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import { createAmountLable, requireDocument, } from 'utils';
import { fetchOrderDraft, submitOrderDraft, } from 'sources/order-draft/actions';
import QueryLink from './QueryLink';
import internalSites from 'resources/configs/internalSites.json';
import { replaceItem, } from 'sources/item/actions';

class OrderDraft extends Component {
    constructor(props) {
        super(props);
        const { query, orderDraft, onRequireOrderDraft, } = this.props;
        requireDocument(orderDraft, query.draftID, onRequireOrderDraft);
    }

    render() {
        const {
            language, orderDraft, currency, onClickItemName, onSubmitOrderDraft,
        } = this.props;
        if (!orderDraft)return null;

        return h('div', {
            children: [
                h('div', {
                    key: 0,
                    children: orderDraft.purchaseRecords.map(({ item, count, }, key) =>
                        h('div', {
                            key,
                            children: [
                                h('img', {
                                    key: 0,
                                    src: item.img,
                                }),
                                h(QueryLink, {
                                    key: 1,
                                    to: internalSites.item.$url,
                                    query: {
                                        itemID: item.id,
                                    },
                                    onClick: () => onClickItemName && onClickItemName(item),
                                    children: item.name,
                                }),
                                h('span', {
                                    key: 2,
                                    children: createAmountLable(item.price, currency, language),
                                }),
                                h('span', {
                                    key: 3,
                                    children: count,
                                }),
                                h('span', {
                                    key: 4,
                                    children: createAmountLable((item.price * count).toFixed(2), currency, language),
                                }),
                            ],
                        })),
                }),
                h('div', {
                    key: 1,
                    onClick: () => onSubmitOrderDraft && onSubmitOrderDraft(orderDraft),
                    children: '提交订单',
                }),
            ],
        });
    }
}

const mapStateToProps = ({ language, orderDraft, query, currency, }) => ({
    language,
    orderDraft,
    query,
    currency,
});

const mapDispatchToProps = dispatch => ({
    onRequireOrderDraft: (draftID) => dispatch(fetchOrderDraft(draftID)),
    onClickItemName: (item) => dispatch(replaceItem(item)),
    onSubmitOrderDraft: (draft) => dispatch(submitOrderDraft(draft)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDraft);