/**
 * Created by Administrator on 2017.12.15.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import { fetchOrder, } from 'sources/order/actions';
import { formatDate, createAmountLable, requireDocument, } from 'utils';

class Order extends Component {
    constructor(props) {
        super(props);
        const { onRequireOrder, query, order, } = this.props;
        requireDocument(order, query.orderID, onRequireOrder);
    }

    render() {
        const { language, order, } = this.props;
        if (!order)return null;
        let button = null;
        switch (order.state) {
            case 'successfulTrade':
                button = h('input', {
                    type: 'button',
                    value: language.additionallyEvaluate,
                });
                break;
            case 'pendingConfirming':
                button = h('input', {
                    type: 'button',
                    value: language.confirm,
                });
                break;
            case 'pendingDelivery':
                button = h('input', {
                    type: 'button',
                    value: language.remindDelivery,
                });
                break;
            case 'pendingPayment':
                button = h('input', {
                    type: 'button',
                    value: language.pay,
                });
                break;
            case 'pendingEvaluating':
                button = h('input', {
                    type: 'button',
                    value: language.evaluate,
                });
                break;
        }
        return h('div', {
            children: [
                h('div', {
                    key: 0,
                    children: [
                        h('div', {
                            key: 0,
                            children: language[ order.state ],
                        }),
                        h('div', {
                            key: 1,
                            children: button,
                        }),
                    ],
                }),
                h('div', {
                    key: 1,
                    children: [
                        h('div', {
                            key: 0,
                            children: language.orderInfo,
                        }),
                        h('div', {
                            key: 1,
                            children: [
                                h('span', {
                                    key: 0,
                                    children: language.address,
                                }),
                                h('span', {
                                    key: 1,
                                    children: order.address.area + order.address.detailAddress,
                                }),
                            ],
                        }),
                        h('div', {
                            key: 2,
                            children: [
                                h('span', {
                                    key: 0,
                                    children: language.deliveryType,
                                }),
                                h('span', {
                                    key: 1,
                                    children: language[ order.item.deliveryType ],
                                }),
                            ],
                        }),
                        h('div', {
                            key: 3,
                            children: [
                                h('div', {
                                    key: 0,
                                    children: language.sellerInfo,
                                }),
                                h('div', {
                                    key: 1,
                                    children: [
                                        h('span', {
                                            key: 0,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.nickname,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: order.customer.nickname,
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 3,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.phoneNumber,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: order.customer.phone,
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 4,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.email,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: order.customer.email,
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 5,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.alipay,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: order.customer.alipay,
                                                }),
                                            ],
                                        }),
                                    ],
                                }),
                            ],
                        }),
                        h('div', {
                            key: 4,
                            children: [
                                h('div', {
                                    key: 0,
                                    children: language.orderInfo,
                                }),
                                h('div', {
                                    key: 1,
                                    children: [
                                        h('span', {
                                            key: 0,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.draftID,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: order.id,
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 2,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.createdAt,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: formatDate(order.submittedAt, 'yyyy-MM-dd hh:mm:ss'),
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 3,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.deliveredAt,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: formatDate(order.deliveredAt, 'yyyy-MM-dd hh:mm:ss'),
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 4,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.payedAt,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: formatDate(order.payedAt, 'yyyy-MM-dd hh:mm:ss'),
                                                }),
                                            ],
                                        }),
                                        h('span', {
                                            key: 5,
                                            children: [
                                                h('span', {
                                                    key: 0,
                                                    children: language.dealtAt,
                                                }),
                                                h('span', {
                                                    key: 1,
                                                    children: formatDate(order.dealtAt, 'yyyy-MM-dd hh:mm:ss'),
                                                }),
                                            ],
                                        }),
                                    ],
                                }),
                            ],
                        }),
                        h('div', {
                            key: 5,
                            children: [
                                h('div', {
                                    key: 0,
                                    children: language.amount,
                                }),
                                h('div', {
                                    key: 1,
                                    children: createAmountLable(order.amount, order.currency, language),
                                }),
                            ],
                        }),
                    ],
                }),
            ],
        });
    }
}

const mapStateToProps = ({ language, order, query, }) => ({
    language,
    order,
    query,
});

const mapDispatchToProps = dispatch => ({
    onRequireOrder: (orderID) => dispatch(fetchOrder(orderID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Order);