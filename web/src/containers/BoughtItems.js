/**
 * Created by isbdnt on 2017/12/13.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import { TabSwitcher, } from 'components';
import OrderProfile from './OrderProfile';
import { fetchOrders, } from 'sources/orders/actions';
import QueryLink from './QueryLink';
import internalSites from 'resources/configs/internalSites.json';

class BoughtItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            all: 0,
            pendingConfirming: 0,
            pendingDelivery: 0,
            pendingPayment: 0,
            pendingEvaluating: 0,
        };
    }

    getTab = (option) => {
        const { orders, onRequireOrders, language, } = this.props;
        return {
            label: h(QueryLink, {
                to: internalSites.user.boughtItems.$url,
                query: {
                    collectionType: option,
                },
                children: language[ option ],
            }),
            render: () => {
                const profiles = (orders[ option ] || {})[ this.state[ option ] ];
                if (!profiles)
                    onRequireOrders && onRequireOrders(option, this.state[ option ]);
                return h('div', {
                    children: ( profiles || [])
                        .map((order, key) => h(OrderProfile, { key, order, })),
                });
            },
            option,
        };
    };

    render() {
        const { query, } = this.props;
        const tabs = [
            this.getTab('all'),
            this.getTab('pendingConfirming'),
            this.getTab('pendingDelivery'),
            this.getTab('pendingPayment'),
            this.getTab('pendingEvaluating'),
        ];
        return h(TabSwitcher, {
            tabs,
            selectedTab: query.collectionType || 'all',
        });
    }
}

const mapStateToProps = ({ language, orders, query, }) => ({
    language,
    orders,
    query,
});

const mapDispatchToProps = dispatch => ({
    onRequireOrders: (collectionType, page) => setImmediate(() => dispatch(fetchOrders(collectionType, page))),
});

export default connect(mapStateToProps, mapDispatchToProps)(BoughtItems);