/**
 * Created by isbdnt on 2017/12/17.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { Link, } from 'react-router-dom';
import qs from 'qs';
import { connect, } from 'react-redux';
import { pushQuery, } from 'sources/query/actions';

class QueryLink extends Component {
    render() {
        const { to, query, onPathChange, children, onClick, } = this.props;
        return h(Link, {
            to: `${to}?${qs.stringify(query)}`,
            onClick: () => onPathChange(query) && onClick && onClick(),
            children,
        });
    }
}

const mapStateToProps = ({ language, }) => ({
    language,
});

const mapDispatchToProps = dispatch => ({
    onPathChange: query => dispatch(pushQuery(query)),
});

export default connect(mapStateToProps, mapDispatchToProps)(QueryLink);