/**
 * Created by isbdnt on 2017/12/12.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { BisectedBar, Dropdown, } from 'components';
import { connect, } from 'react-redux';
import _ from 'lodash';
import { updateRegion, } from 'sources/region/actions';
import internalSites from 'resources/configs/internalSites.json';
import externalSites from 'resources/configs/externalSites.json';
import { Link, } from 'react-router-dom';

class NavBar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { language, user, shoppingCartRecords, path, region, } = this.props;
        return h(BisectedBar, {
            left: [
                h(Dropdown, {
                    key: 0,
                    popup: _.map(language.regions, (opt, key) =>
                        h('div', {
                            key,
                            onClick: () => this.props.onClickRegion(key),
                            children: opt,
                        })),
                    children: language.regions[ region ],
                }),
                ...(user ?
                    [
                        h(Dropdown, {
                            key: 1,
                            popup: h('div', 'user'),
                            children: h(Link, { to: internalSites.user.$url, children: user.nickname, }),
                        }),
                        h(Dropdown, {
                            key: 2,
                            popup: h('div', 'messages'),
                            children: language.messages,
                        }),
                    ] :
                    [
                        h('span', {
                            key: 1,
                            children: h(Link, { to: internalSites.signIn.$url, children: language.signIn, }),
                        }),
                        h('span', {
                            key: 2,
                            children: h(Link, { to: internalSites.signUp.$url, children: language.signUp, }),
                        }),
                    ]),
                h('span', {
                    key: 3,
                    children: h(Link, {
                        to: internalSites.downloadMobile.$url, children: language.downloadMobile,
                    }),
                }),
            ],
            right: [
                path !== '/' ? h('span', {
                    key: -1,
                    children: h(Link, {
                        to: '/', children: language.index,
                    }),
                }) : null,
                h(Dropdown, {
                    key: 0,
                    popup: [
                        { label: language.boughtItems, to: internalSites.user.boughtItems.$url, },
                        { label: language.footstep, to: internalSites.user.footstep.$url, },
                    ].map(({ label, to, }, key) =>
                        h('div', {
                            key,
                            children: h(Link, {
                                to, children: label,
                            }),
                        })),
                    children: h(Link, { to: internalSites.user.$url, children: language.mine, }),
                }),
                h(Dropdown, {
                    key: 1,
                    popup: shoppingCartRecords.map((col, key) =>
                        h('div', {
                            key,
                            children: 'item column',
                        })),
                    children: h(Link, {
                        to: internalSites.shoppingCart.$url,
                        children: language.shoppingCart,
                    }),
                }),
                h(Dropdown, {
                    key: 2,
                    popup: [
                        { label: language.collectedItems, to: internalSites.user.collectedItems.$url, },
                        { label: language.collectedShop, to: internalSites.user.collectedShop.$url, },
                    ].map(({ label, to, }, key) =>
                        h('div', {
                            key,
                            children: h(Link, {
                                to, children: label,
                            }),
                        })),
                    children: h(Link, {
                        to: internalSites.user.collectionList.$url, children: language.collectionList,
                    }),
                }),
                h('span', {
                    key: 3,
                    children: h(Link, {
                        to: internalSites.marketList.$url, children: language.marketList,
                    }),
                }),
                h(Dropdown, {
                    key: 4,
                    popup: [
                        { label: language.openShop, to: internalSites.openShop.$url, },
                        { label: language.soldItems, to: internalSites.user.soldItems.$url, },
                    ].map(({ label, to, }, key) =>
                        h('div', {
                            key,
                            children: h(Link, {
                                to, children: label,
                            }),
                        })),
                    children: h(Link, {
                        to: internalSites.sellerCenter, children: language.sellerCenter,
                    }),
                }),
                h(Dropdown, {
                    key: 5,
                    popup: [
                        { label: language.customerService, to: internalSites.customerService, },
                        { label: language.sellerService, to: internalSites.sellerService, },
                    ].map(({ label, to, }, key) =>
                        h('div', {
                            key,
                            children: h(Link, {
                                to, children: label,
                            }),
                        })),
                    children: h(Link, {
                        to: internalSites.contactService, children: language.contactService,
                    }),
                }),
                h(Dropdown, {
                    key: 6,
                    popup: [
                        { label: language.alipay, href: externalSites.alipay, },
                        { label: language.tmall, href: externalSites.tmall, },
                    ].map(({ label, href, }, key) =>
                        h('div', {
                            key,
                            children: h('a', {
                                href, children: label,
                            }),
                        })),
                    children: h(Link, {
                        to: internalSites.sitemap, children: language.sitemap,
                    }),
                }),
            ],
        });
    }
}

const mapStateToProps = ({ language, user, shoppingCartRecords, region, }) => ({
    language,
    user,
    shoppingCartRecords,
    region,
});

const mapDispatchToProps = dispatch => ({
    onClickRegion: region => dispatch(updateRegion(region)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);