/**
 * Created by isbdnt on 2017/12/13.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { Search, } from 'components';
import { connect, } from 'react-redux';
import 'rxjs/add/operator/takeUntil';
import { fetchSearchAdvices, } from 'sources/search-advices/actions';
import QueryLink from './QueryLink';
import internalSites from 'resources/configs/internalSites.json';
import _ from 'lodash';

class AdvisedSearch extends Component {
    render() {
        const { language, onRequireSearchAdvices, searchAdvices, } = this.props;
        return h(Search, {
            button: keyword => h(QueryLink, {
                to: internalSites.search.$url,
                query: {
                    keyword,
                },
                children: language.search,
            }),
            onKeywordChange: (keyword) =>
            onRequireSearchAdvices && onRequireSearchAdvices(keyword),
            advices: searchAdvices,
        });
    }
}


const mapStateToProps = ({ language, searchAdvices, }) => ({
    language,
    searchAdvices,
});

const mapDispatchToProps = (dispatch) => ({
    onRequireSearchAdvices: _.throttle((keyword) =>
        setImmediate(() => dispatch(fetchSearchAdvices(keyword))), 500, { trailing: true, }),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdvisedSearch);