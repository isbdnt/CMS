/**
 * Created by isbdnt on 2017/12/14.
 */
import { Component, } from 'react';
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import { formatDate, createAmountLable, } from 'utils';
import internalSites from 'resources/configs/internalSites.json';
import QueryLink from './QueryLink';
import { replaceOrder, } from 'sources/order/actions';
import { replaceItem, } from 'sources/item/actions';

class OrderProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { language, order, onClickOrderDetails,onClickItemName, } = this.props;
        const item = order.item;
        let lastCol = null;
        switch (order.state) {
            case 'successfulTrade':
                lastCol = h('input', {
                    type: 'button',
                    value: language.additionallyEvaluate,
                });
                break;
            case 'pendingConfirming':
                lastCol = h('input', {
                    type: 'button',
                    value: language.confirm,
                });
                break;
            case 'pendingDelivery':
                lastCol = h('input', {
                    type: 'button',
                    value: language.remindDelivery,
                });
                break;
            case 'pendingPayment':
                lastCol = h('input', {
                    type: 'button',
                    value: language.pay,
                });
                break;
            case 'pendingEvaluating':
                lastCol = h('input', {
                    type: 'button',
                    value: language.evaluate,
                });
                break;
        }
        return h('div', {
            style: {
                display: 'flex',
                flexDirection: 'column',
            },
            children: [
                h('div', {
                    key: 0,
                    children: [
                        h('span', {
                            key: 0,
                            children: formatDate(order.submittedAt, 'yyyy-MM-dd'),
                        }),
                        h('span', {
                            key: 1,
                            children: order.id,
                        }),
                    ],
                }),
                h('div', {
                    key: 1,
                    style: {
                        display: 'flex',
                    },
                    children: [
                        h('img', {
                            key: 0,
                            src: item.img,
                        }),
                        h(QueryLink, {
                            key: 1,
                            to: internalSites.item.$url,
                            query: {
                                itemID: item.id,
                            },
                            onClick: () => onClickItemName && onClickItemName(item),
                            children: item.name,
                        }),
                        h('span', {
                            key: 2,
                            children: createAmountLable(item.price, order.currency, language),
                        }),
                        h('span', {
                            key: 3,
                            children: order.count,
                        }),
                        h('span', {
                            key: 4,
                            children: createAmountLable(order.amount, order.currency, language),
                        }),
                        h('span', {
                            key: 5,
                            children: [
                                h('div', {
                                    key: 0,
                                    children: language[ order.state ],
                                }),
                                h('div', {
                                    key: 1,
                                    children: h(QueryLink, {
                                        to: internalSites.order.$url,
                                        query: { orderID: order.id, },
                                        onClick: () => onClickOrderDetails(order),
                                        children: language.orderDetails,
                                    }),
                                }),
                            ],
                        }),
                        h('span', {
                            key: 6,
                            children: lastCol,
                        }),
                    ],
                }),
            ],
        });
    }
}

const mapStateToProps = ({ language, }) => ({
    language,
});

const mapDispatchToProps = dispatch => ({
    onClickOrderDetails: (order) => dispatch(replaceOrder(order)),
    onClickItemName: (item) => dispatch(replaceItem(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderProfile);