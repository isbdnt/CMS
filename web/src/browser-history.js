/**
 * Created by isbdnt on 2017/12/24.
 */
import createHistory from 'history/createBrowserHistory';

export default createHistory();