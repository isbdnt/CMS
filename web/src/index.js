/**
 * Created by isbdnt on 2017/12/11.
 */
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer, } from 'react-hot-loader';
import App from 'App';
import fetchInitialData from 'fetchInitialData';

fetchInitialData();

const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <Component/>
        </AppContainer>,
        document.getElementById('App')
    );
};

render(App);

// Webpack Hot Module Replacement API
if (module.hot) {
    module.hot.accept('App', () => render(require('App').default));
}