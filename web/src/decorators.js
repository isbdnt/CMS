/**
 * Created by isbdnt on 2017/12/15.
 */
import { Observable, Subject, } from 'rxjs';
Observable.timer();

export const ObserveLifecycle = (component) => {
    Object.assign(component.prototype, {
        onComponentWillUnmount: new Subject(),
        componentWillUnmount(){
            this.onComponentWillUnmount.next();
            this.onComponentWillUnmount.complete();
        },
    });
};
