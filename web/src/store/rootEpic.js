/**
 * Created by isbdnt on 2017/12/12.
 */
import { combineEpics, } from 'redux-observable';
import language from 'sources/language/epic';
import user from 'sources/user/epic';
import orders from 'sources/orders/epic';
import order from 'sources/order/epic';
import searchAdvices from 'sources/search-advices/epic';
import items from 'sources/items/epic';
import item from 'sources/item/epic';
import shoppingCartRecords from 'sources/shopping-cart-records/epic';
import orderDraft from 'sources/order-draft/epic';
import payment from 'sources/payment/epic';
export default combineEpics(
    language,
    user,
    orders,
    order,
    searchAdvices,
    items,
    item,
    shoppingCartRecords,
    orderDraft,
    payment
);