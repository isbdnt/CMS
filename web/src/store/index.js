/**
 * Created by isbdnt on 2017/12/12.
 */
import { createStore, applyMiddleware, compose, } from 'redux';
import { createEpicMiddleware, } from 'redux-observable';
import rootEpic from './rootEpic';
import rootReducer from './rootReducer';

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const epicMiddleware = createEpicMiddleware(rootEpic);
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(epicMiddleware)
));

if (module.hot) {
    module.hot.accept('./rootReducer', () => {
        store.replaceReducer(require('./rootReducer').default);
        require('fetchInitialData').default();
    });
    module.hot.accept('./rootEpic', () => {
        epicMiddleware.replaceEpic(require('./rootEpic').default);
        require('fetchInitialData').default();
    });
}

export default store;