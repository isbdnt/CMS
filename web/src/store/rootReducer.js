/**
 * Created by isbdnt on 2017/12/12.
 */
import { combineReducers, } from 'redux';
import language from 'sources/language/reducer';
import user from 'sources/user/reducer';
import shoppingCartRecords from 'sources/shopping-cart-records/reducer';
import orders from 'sources/orders/reducer';
import order from 'sources/order/reducer';
import searchAdvices from 'sources/search-advices/reducer';
import query from 'sources/query/reducer';
import items from 'sources/items/reducer';
import item from 'sources/item/reducer';
import region from 'sources/region/reducer';
import currency from 'sources/currency/reducer';
import orderDraft from 'sources/order-draft/reducer';
import payment from 'sources/payment/reducer';
export default combineReducers({
    language,
    user,
    shoppingCartRecords,
    orders,
    order,
    searchAdvices,
    query,
    items,
    item,
    region,
    currency,
    orderDraft,
    payment,
});