/**
 * Created by isbdnt on 2017/12/25.
 */
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import history from 'browser-history';

export default (Component) =>
    connect(({ user, }) => ({ user, }))(({ user, }) => {
        if (user) {
            history.push('/');
            return null;
        }
        return h(Component);
    });