/**
 * Created by isbdnt on 2017/12/25.
 */
import renderWithLanguage from './renderWithLanguage';
import renderWithUser from './renderWithUser';
import renderWithoutUser from './renderWithoutUser';
export {
    renderWithLanguage,
    renderWithUser,
    renderWithoutUser,
};