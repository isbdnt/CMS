/**
 * Created by isbdnt on 2017/12/25.
 */
import h from 'react-hyperscript';
import { connect, } from 'react-redux';
import history from 'browser-history';
import internalSites from 'resources/configs/internalSites.json';

export default (Component) =>
    connect(({ user, }) => ({ user, }))(({ user, }) => {
        if (!user) {
            history.push(`${internalSites.signIn.$url}?${history.location.search}`);
            return null;
        }
        return h(Component);
    });