/**
 * Created by isbdnt on 2017/12/12.
 */
import store from 'store';
import { fetchLanguage, } from 'sources/language/actions';
import { fetchUser, } from 'sources/user/actions';
import { pushQuery, } from 'sources/query/actions';
import history from 'browser-history';
import { getWindowQuery, } from 'utils';

history.listen(() => store.dispatch(pushQuery(getWindowQuery())));

export default () => {
    store.dispatch(pushQuery(getWindowQuery()));
    store.dispatch(fetchLanguage(store.getState().region));
    store.dispatch(fetchUser());
};
