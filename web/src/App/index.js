/**
 * Created by isbdnt on 2017/12/11.
 */
import React, { Component, } from 'react';
import {
    IndexPage, SignInPage, SignUpPage, UserPage, OrderPage,
    SearchPage, ItemPage, DraftOrderPage, ShoppingCartPage,
    PaymentPage,
} from 'web-pages';
import { Provider, } from 'react-redux';
import store from 'store';
import internalSites from 'resources/configs/internalSites.json';
import {
    Router,
    Route,
} from 'react-router-dom';
import history from 'browser-history';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={history}>
                    <div>
                        <Route exact path="/" component={IndexPage}/>
                        <Route path={internalSites.signIn.$url} component={SignInPage}/>
                        <Route path={internalSites.signUp.$url} component={SignUpPage}/>
                        <Route path={internalSites.user.$url} component={UserPage}/>
                        <Route path={internalSites.order.$url} component={OrderPage}/>
                        <Route path={internalSites.search.$url} component={SearchPage}/>
                        <Route path={internalSites.item.$url} component={ItemPage}/>
                        <Route path={internalSites.orderDraft.$url} component={DraftOrderPage}/>
                        <Route path={internalSites.shoppingCart.$url} component={ShoppingCartPage}/>
                        <Route path={internalSites.payment.$url} component={PaymentPage}/>
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;