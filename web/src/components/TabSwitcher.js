/**
 * Created by isbdnt on 2017/12/14.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import PropTypes from 'prop-types';

export default class TabSwitcher extends Component {
    static propTypes = {
        tabs: PropTypes.arrayOf(PropTypes.shape({
            render: PropTypes.func.isRequired,
            option: PropTypes.string.isRequired,
        })).isRequired,
        selectedTab: PropTypes.string,
    };

    constructor(props) {
        super(props);
        const { selectedTab, } = props;
        this.state = {
            selectedTab,
        };
    }

    render() {
        const { tabs, } = this.props;
        const tab = tabs.find(tab => tab.option === this.state.selectedTab);
        return h('span', {
            style: {
                display: 'flex',
                flexDirection: 'column',
            },
            children: [
                h('div', {
                    key: 0,
                    children: tabs.map(({ label, option, }, key) => h('span', {
                        key,
                        onClick: () => this.setState({ selectedTab: option, }),
                        children: label,
                    })),
                }),
                tab ? h('div', {
                    key: 1,
                    children: tab.render(),
                }) : null,
            ],
        });
    }
}
