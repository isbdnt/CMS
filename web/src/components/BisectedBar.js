/**
 * Created by isbdnt on 2017/12/12.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';

export default class BisectedBar extends Component {
    render() {
        return h('div', {
            style: {
                display: 'flex',
                justifyContent: 'space-between',
            },
            children: [
                h('div', {
                    key: 0,
                    children: this.props.left,
                }),
                h('div', {
                    key: 1,
                    children: this.props.right,
                }),
            ],
        });
    }
}
