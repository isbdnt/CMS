/**
 * Created by isbdnt on 2017/12/12.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';

export default class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hoverLabel: false,
            hoverPopup: false,
        };
    }

    render() {
        return h('span', {
            children: [
                h('span', {
                    key: 0,
                    onMouseEnter: () => this.setState({ hoverLabel: true, }),
                    onMouseLeave: () => this.setState({ hoverLabel: false, }),
                    ref: 'anchor',
                    children: this.props.children,
                }),
                this.state.hoverLabel || this.state.hoverPopup ?
                    h('span', {
                        key: 1,
                        onMouseEnter: () => this.setState({ hoverPopup: true, }),
                        onMouseLeave: () => this.setState({ hoverPopup: false, }),
                        style: {
                            position: 'absolute',
                            left: this.refs.anchor.offsetLeft,
                            top: this.refs.anchor.offsetTop + this.refs.anchor.offsetHeight,
                            backgroundColor: '#fff',
                            zIndex: 100,
                        },
                        children: this.props.popup,
                    }) : null,
            ],
        });
    }
}
