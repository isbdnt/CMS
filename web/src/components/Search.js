/**
 * Created by isbdnt on 2017/12/13.
 */
import  { Component, } from 'react';
import h from 'react-hyperscript';
import { Link, } from 'react-router-dom';

export default class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            keyword: "",
        };
    }

    componentDidMount = () => {
        document.addEventListener("click", this.tryClosePopup);
    };
    componentWillUnmount = () => {
        document.removeEventListener("click", this.tryClosePopup);
    };

    tryClosePopup = (e) => {
        if (e.target !== this.refs.anchor) {
            this.setState({ open: false, });
        }
    };

    handleChange = event => {
        this.props.onKeywordChange && this.props.onKeywordChange(event.target.value);
        this.setState({ keyword: event.target.value });
    };

    render() {
        const { advices, button, } = this.props;
        return h('span', {
            children: [
                h('input', {
                    key: 0,
                    ref: 'anchor',
                    onChange: this.handleChange,
                    onClick: () => this.setState({ open: true, }),
                    children: this.props.children,
                }),
                h('span', {
                    key: 1,
                    children: button(this.state.keyword),
                }),
                this.state.open ?
                    h('span', {
                        key: 2,
                        style: {
                            position: 'absolute',
                            left: this.refs.anchor.offsetLeft,
                            top: this.refs.anchor.offsetTop + this.refs.anchor.offsetHeight,
                            backgroundColor: '#fff',
                            zIndex: 100,
                        },
                        children: advices.map((advice, key) =>
                            h('div', {
                                key,
                                children: h(Link, {
                                    to: '/',
                                    onClick: this.tryClosePopup,
                                    children: advice,
                                }),
                            })),
                    }) : null,
            ],
        });
    }
}
