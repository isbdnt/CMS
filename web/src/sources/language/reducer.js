/**
 * Created by isbdnt on 2017/12/12.
 */
import { FETCH_LANGUAGE, FETCH_LANGUAGE_FULFILLED, } from './action-types';

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_LANGUAGE:
            return state;
        case FETCH_LANGUAGE_FULFILLED:
            return action.language;
        default:
            return state;
    }
};