/**
 * Created by isbdnt on 2017/12/12.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { fetchLanguage, fetchLanguageFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import { FETCH_LANGUAGE, } from './action-types';
import { UPDATE_REGION, } from 'sources/region/action-types';
import languages from 'api/v1-0/languages';

const regionEpic = (action$) =>
    action$.ofType(UPDATE_REGION)
        .map(({ region, }) => fetchLanguage(region));

const fetchLanguageEpic = action$ =>
    action$.ofType(FETCH_LANGUAGE)
        .mergeMap(({ region, }) =>
            languages.get({
                region,
            })
                .map(({ data, }) => fetchLanguageFulfilled(data)))
        .catch(e => console.error(e));

export default combineEpics(
    regionEpic,
    fetchLanguageEpic
);