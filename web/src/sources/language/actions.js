/**
 * Created by Administrator on 2017.12.15.
 */
import {
    FETCH_LANGUAGE,
    FETCH_LANGUAGE_FULFILLED,
} from './action-types';

export const fetchLanguage = (region) => ({
    type: FETCH_LANGUAGE,
    region,
});

export const fetchLanguageFulfilled = (language) => ({
    type: FETCH_LANGUAGE_FULFILLED,
    language,
});