/**
 * Created by isbdnt on 2017/12/13.
 */
import {
    ADD_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    UPDATE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    REMOVE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
} from './action-types';

export default (state = [], action) => {
    switch (action.type) {
        case ADD_SHOPPING_CART_PURCHASE_RECORD_FULFILLED:
            return [ ...state, action.record, ];
        case UPDATE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED:
            const index = state.findIndex(record => record.id === action.record.id);
            return [
                ...state.slice(0, index),
                action.record,
                ...state.slice(index + 1),
            ];
        case REMOVE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED:
            return state.filter(record => record.id !== action.recordID);
        default:
            return state;
    }
};