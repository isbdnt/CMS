/**
 * Created by Administrator on 2017.12.15.
 */
import {
    ADD_ITEM_TO_SHOPPING_CART,
    ADD_SHOPPING_CART_RECORD,
    ADD_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    UPDATE_SHOPPING_CART_RECORD,
    UPDATE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    REMOVE_SHOPPING_CART_RECORD,
    REMOVE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
} from './action-types';

export const addItemToShoppingCart = (item, count) => ({
    type: ADD_ITEM_TO_SHOPPING_CART,
    item,
    count,
});

export const addShoppingCartRecord = (item, count) => ({
    type: ADD_SHOPPING_CART_RECORD,
    item,
    count,
});

export const addShoppingCartRecordFulfilled = (record) => ({
    type: ADD_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    record,
});

export const updateShoppingCartRecord = (recordID, patch) => ({
    type: UPDATE_SHOPPING_CART_RECORD,
    recordID,
    patch,
});

export const updateShoppingCartRecordFulfilled = (record) => ({
    type: UPDATE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    record,
});

export const removeShoppingCartRecord = (recordID) => ({
    type: REMOVE_SHOPPING_CART_RECORD,
    recordID,
});

export const removeShoppingCartRecordFulfilled = (recordID) => ({
    type: REMOVE_SHOPPING_CART_PURCHASE_RECORD_FULFILLED,
    recordID,
});