/**
 * Created by isbdnt on 2017/12/24.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import shoppingCartRecords from 'api/v1-0/users/shopping-cart-records/index';
import {
    ADD_SHOPPING_CART_RECORD,
    UPDATE_SHOPPING_CART_RECORD,
    REMOVE_SHOPPING_CART_RECORD,
    ADD_ITEM_TO_SHOPPING_CART,
} from './action-types';
import {
    addShoppingCartRecordFulfilled,
    updateShoppingCartRecordFulfilled,
    removeShoppingCartRecordFulfilled,
    updateShoppingCartRecord,
    addShoppingCartRecord,
    removeShoppingCartRecord,
} from './actions';
import { combineEpics, } from 'redux-observable';
import { CREATE_ORDER_DRAFT_FULFILLED, } from 'sources/order-draft/action-types';
import { UPDATE_COUNT } from './update-types';

const addShoppingCartRecordEpic = (action$, store) =>
    action$.ofType(ADD_SHOPPING_CART_RECORD)
        .mergeMap(({ item, count, }) =>
            shoppingCartRecords.post({
                userID: store.getState().user.id,
                itemID: item.id,
                count,
            })
                .map(({ data, }) => addShoppingCartRecordFulfilled(data)))
        .catch(e => console.error(e));

const updateShoppingCartRecordEpic = (action$, store) =>
    action$.ofType(UPDATE_SHOPPING_CART_RECORD)
        .mergeMap(({ recordID, patch, }) =>
            shoppingCartRecords.patch({
                userID: store.getState().user.id,
                recordID,
                ...patch,
            })
                .map(({ data, }) => updateShoppingCartRecordFulfilled(data)))
        .catch(e => console.error(e));

const removeShoppingCartRecordEpic = (action$, store) =>
    action$.ofType(REMOVE_SHOPPING_CART_RECORD)
        .mergeMap(({ recordID, }) =>
            shoppingCartRecords.remove({
                userID: store.getState().user.id,
                recordID,
            })
                .map(() => removeShoppingCartRecordFulfilled(recordID)))
        .catch(e => console.error(e));

const addItemToShoppingCartEpic = (action$, store) =>
    action$.ofType(ADD_ITEM_TO_SHOPPING_CART)
        .map(({ item, count, }) => {
            const record = store.getState().shoppingCartRecords.find(record => record.item.id === item.id);
            if (record) {
                return updateShoppingCartRecord(record.id, {
                    type: UPDATE_COUNT,
                    count: record.count + count,
                });
            }
            return addShoppingCartRecord(item, count);
        })
        .catch(e => console.error(e));

const createOrderDraftFulfilledEpic = (action$) =>
    action$.ofType(CREATE_ORDER_DRAFT_FULFILLED)
        .flatMap(({ recordIDs, }) => recordIDs)
        .map((recordID) =>
            removeShoppingCartRecord(recordID))
        .catch(e => console.error(e));

export default combineEpics(
    addShoppingCartRecordEpic,
    updateShoppingCartRecordEpic,
    removeShoppingCartRecordEpic,
    addItemToShoppingCartEpic,
    createOrderDraftFulfilledEpic
);