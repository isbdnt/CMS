/**
 * Created by Administrator on 2017.12.15.
 */
import {
    FETCH_ORDERS,
    FETCH_ORDERS_FULFILLED,
    CREATE_ORDERS, CREATE_ORDERS_FULFILLED,
    USER_PAY_ORDERS, USER_PAY_ORDERS_FULFILLED,
    CLEAR_ORDERS,
} from './action-types';

export const fetchOrders = (collectionType, page) => ({
    type: FETCH_ORDERS,
    collectionType,
    page,
});

export const fetchOrdersFulfilled = (collectionType, page, orders) => ({
    type: FETCH_ORDERS_FULFILLED,
    collectionType,
    page,
    orders,
});

export const createOrders = (drafts) => ({
    type: CREATE_ORDERS,
    drafts,
});

export const createOrdersFulfilled = (orders) => ({
    type: CREATE_ORDERS_FULFILLED,
    orders,
});

export const userPayOrders = (paymentPassword, orderIDs) => ({
    type: USER_PAY_ORDERS,
    paymentPassword,
    orderIDs,
});

export const userPayOrdersFulfilled = (orders) => ({
    type: USER_PAY_ORDERS_FULFILLED,
    orders,
});

export const clearOrders = () => ({
    type: CLEAR_ORDERS,
});