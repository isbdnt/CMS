/**
 * Created by isbdnt on 2017/12/14.
 */
import {
    FETCH_ORDERS, FETCH_ORDERS_FULFILLED,
    CLEAR_ORDERS,
} from './action-types';

export default (state = {}, action) => {
    switch (action.type) {
        case CLEAR_ORDERS:
            return {};
        case FETCH_ORDERS:
            return state;
        case FETCH_ORDERS_FULFILLED:
            return {
                ...state,
                [action.collectionType]: {
                    ...state[ action.collectionType ],
                    [action.page]: action.orders,
                },
            };
        default:
            return state;
    }
};