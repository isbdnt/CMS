/**
 * Created by isbdnt on 2017/12/14.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/throttle';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/catch';
import { getIntervalObservable, } from 'utils';
import { fetchOrdersFulfilled, createOrdersFulfilled, userPayOrdersFulfilled, clearOrders, } from './actions';
import { combineEpics, } from 'redux-observable';
import { FETCH_ORDERS, CREATE_ORDERS, USER_PAY_ORDERS, } from './action-types';
import numericalValues from 'resources/configs/numerical-values.json';
import userOrders from 'api/v1-0/users/orders';
import orders from 'api/v1-0/orders';
import { Observable, } from 'rxjs';
import { updatePayment, } from 'sources/payment/actions';
import { pushHistory, } from 'sources/query/actions';
import internalSites from 'resources/configs/internalSites.json';
import { PAY_AMOUNT, } from 'sources/order/update-types';
import { fetchUser, } from 'sources/user/actions';

const fetchOrdersEpic = (action$, store) =>
    action$.ofType(FETCH_ORDERS)
        .throttle(() => getIntervalObservable())
        .mergeMap(({ collectionType, page, }) =>
            userOrders.getMany({
                userID: store.getState().user.id,
                collectionType,
                offset: page * numericalValues.orderPageCount,
                count: numericalValues.orderPageCount,
            })
                .map(({ data, }) => fetchOrdersFulfilled(collectionType, page, data)))
        .catch(e => console.error(e));

const createOrdersEpic = (action$, store) =>
    action$.ofType(CREATE_ORDERS)
        .mergeMap(({ drafts, }) =>
            Observable.of(...drafts)
                .mergeMap(({ itemID, count, addressID, }) =>
                    userOrders.post({
                        userID: store.getState().user.id,
                        addressID: addressID,
                        itemID,
                        count,
                    }))
                .map(({ data, }) => data)
                .toArray())
        .flatMap(orders => [
            createOrdersFulfilled(orders),
            updatePayment({
                amount: orders.reduce((sum, order) => sum + order.amount, 0),
                paid: orders.reduce((sum, order) =>
                sum + (order.state === 'pendingPayment' ? 0 : order.amount), 0),
            }),
            pushHistory(internalSites.payment.$url, {
                orderIDs: orders.map(order => order.id),
            }),
            clearOrders(),
            fetchUser(),
        ])
        .catch(e => console.error(e));

const userPayOrdersEpic = (action$) =>
    action$.ofType(USER_PAY_ORDERS)
        .mergeMap(({ paymentPassword, orderIDs, }) =>
            Observable.of(...orderIDs)
                .mergeMap((orderID) =>
                    orders.patch({
                        orderID,
                        paymentPassword,
                        type: PAY_AMOUNT,
                    }))
                .map(({ data, }) => data)
                .toArray())
        .flatMap((orders) => {
            const paid = orders.reduce((sum, order) => sum + order.amount, 0);
            return [
                userPayOrdersFulfilled(orders),
                updatePayment({
                    amount: paid,
                    paid,
                }),
                clearOrders(),
                fetchUser(),
            ];
        })
        .catch(e => console.error(e));

export default combineEpics(
    fetchOrdersEpic,
    createOrdersEpic,
    userPayOrdersEpic
);