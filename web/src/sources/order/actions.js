/**
 * Created by isbdnt on 2017/12/16.
 */
import {
    FETCH_ORDER, FETCH_ORDER_FULFILLED,
    REPLACE_ORDER, CREATE_ORDER, CREATE_ORDER_FULFILLED,
    USER_PAY_ORDER, USER_PAY_ORDER_FULFILLED,
} from './action-types';

export const fetchOrder = (orderID) => ({
    type: FETCH_ORDER,
    orderID,
});

export const fetchOrderFulfilled = (order) => ({
    type: FETCH_ORDER_FULFILLED,
    order,
});

export const replaceOrder = (order) => ({
    type: REPLACE_ORDER,
    order,
});

export const createOrder = (itemID, count, addressID) => ({
    type: CREATE_ORDER,
    addressID, itemID, count,
});

export const createOrderFulfilled = (order) => ({
    type: CREATE_ORDER_FULFILLED,
    order,
});

export const userPayOrder = (paymentPassword, orderID) => ({
    type: USER_PAY_ORDER,
    paymentPassword,
    orderID,
});

export const userPayOrderFulfilled = (order) => ({
    type: USER_PAY_ORDER_FULFILLED,
    order,
});