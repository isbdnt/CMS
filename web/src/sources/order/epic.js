/**
 * Created by isbdnt on 2017/12/16.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/throttle';
import { getIntervalObservable, } from 'utils';
import orders from 'api/v1-0/orders';
import userOrders from 'api/v1-0/users/orders';
import { FETCH_ORDER, CREATE_ORDER, USER_PAY_ORDER, } from './action-types';
import { fetchOrderFulfilled, createOrderFulfilled, userPayOrderFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import { updatePayment, } from 'sources/payment/actions';
import { pushHistory, } from 'sources/query/actions';
import internalSites from 'resources/configs/internalSites.json';
import { PAY_AMOUNT, } from './update-types';
import { clearOrders } from 'sources/orders/actions';
import { fetchUser } from 'sources/user/actions';

const fetchOrderEpic = (action$) =>
    action$.ofType(FETCH_ORDER)
        .throttle(() => getIntervalObservable())
        .mergeMap(({ orderID, }) =>
            orders.get({
                orderID,
            })
                .map(({ data, }) => fetchOrderFulfilled(data)))
        .catch(e => console.error(e));

const createOrderEpic = (action$, store) =>
    action$.ofType(CREATE_ORDER)
        .mergeMap(({ addressID, itemID, count, }) =>
            userOrders.post({
                userID: store.getState().user.id,
                addressID: addressID,
                itemID,
                count,
            }))
        .flatMap(({ data, }) => [
            createOrderFulfilled(data),
            updatePayment({
                paid: data.state === 'pendingPayment' ? 0 : data.amount,
                amount: data.amount,
            }),
            pushHistory(internalSites.payment.$url, {
                orderID: data.id,
            }),
            clearOrders(),
            fetchUser(),
        ])
        .catch(e => console.error(e));

const userPayOrderEpic = (action$) =>
    action$.ofType(USER_PAY_ORDER)
        .mergeMap(({ paymentPassword, orderID, }) =>
            orders.patch({
                orderID,
                paymentPassword,
                type: PAY_AMOUNT,
            }))
        .flatMap(({ data, }) => [
            userPayOrderFulfilled(data),
            updatePayment({
                paid: data.amount,
                amount: data.amount,
            }),
            clearOrders(),
            fetchUser(),
        ])
        .catch(e => console.error(e));

export default combineEpics(
    fetchOrderEpic,
    createOrderEpic,
    userPayOrderEpic
);