/**
 * Created by isbdnt on 2017/12/16.
 */
import {
    FETCH_ORDER_FULFILLED,
    REPLACE_ORDER, CREATE_ORDER_FULFILLED,
} from './action-types';


export default (state = null, action) => {
    switch (action.type) {
        case CREATE_ORDER_FULFILLED:
        case FETCH_ORDER_FULFILLED:
        case REPLACE_ORDER:
            return action.order;
        default:
            return state;
    }
};