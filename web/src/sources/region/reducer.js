/**
 * Created by isbdnt on 2017/12/25.
 */
import { UPDATE_REGION, } from './action-types';

export default (state = window.localStorage.getItem('region') || 'cn', action) => {
    switch (action.type) {
        case UPDATE_REGION:
            window.localStorage.setItem('region', action.region);
            return action.region;
        default:
            return state;
    }
};