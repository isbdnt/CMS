/**
 * Created by isbdnt on 2017/12/25.
 */
import { UPDATE_REGION, } from './action-types';

export const updateRegion = (region) => ({
    type: UPDATE_REGION,
    region,
});