/**
 * Created by isbdnt on 2017/12/17.
 */
import { PUSH_QUERY, PUSH_HISTORY, } from './action-types';
import history from 'browser-history';
import qs from 'qs';

export default (state = null, action) => {
    switch (action.type) {
        case PUSH_QUERY:
            return {
                ...state,
                ...action.query,
            };
        case PUSH_HISTORY:
            setImmediate(() => history.push(`${action.url}?${qs.stringify(action.query)}`));
            return state;
        default:
            return state;
    }
};