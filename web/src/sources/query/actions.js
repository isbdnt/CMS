/**
 * Created by isbdnt on 2017/12/17.
 */
import { PUSH_QUERY,PUSH_HISTORY, } from './action-types';

export const pushQuery = (query) => ({
    type: PUSH_QUERY,
    query,
});

export const pushHistory=(url,query)=>({
    type:PUSH_HISTORY,
    url,
    query,
});