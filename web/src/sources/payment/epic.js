/**
 * Created by isbdnt on 2017/12/27.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/throttle';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/catch';
import { getIntervalObservable, } from 'utils';
import { fetchPaymentFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import { FETCH_PAYMENT, USER_PAY_AMOUNT, } from './action-types';
import orders from 'api/v1-0/orders';
import { Observable, } from 'rxjs';
import { userPayOrder, } from 'sources/order/actions';
import { userPayOrders, } from 'sources/orders/actions';

const fetchPaymentAmountEpic = (action$) =>
    action$.ofType(FETCH_PAYMENT)
        .throttle(() => getIntervalObservable())
        .mergeMap(({ orderIDs, }) =>
            Observable.of(...orderIDs)
                .mergeMap((orderID) =>
                    orders.get({
                        orderID,
                    }))
                .map(({ data, }) => data)
                .toArray())
        .map(orders =>
            fetchPaymentFulfilled({
                amount: orders.reduce((sum, order) => sum + order.amount, 0),
                paid: orders.reduce((sum, order) =>
                sum + (order.state === 'pendingPayment' ? 0 : order.amount), 0),
            }))
        .catch(e => console.log(e));

const userPayAmountEpic = (action$) =>
    action$.ofType(USER_PAY_AMOUNT)
        .map(({ paymentPassword, orderIDs, }) => {
            if (orderIDs.length > 1) {
                return userPayOrders(paymentPassword, orderIDs);
            }
            else {
                return userPayOrder(paymentPassword, orderIDs[ 0 ]);
            }
        })
        .catch(e => console.error(e));

export default combineEpics(
    fetchPaymentAmountEpic,
    userPayAmountEpic
);