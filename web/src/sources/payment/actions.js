/**
 * Created by isbdnt on 2017/12/27.
 */
import {
    UPDATE_PAYMENT, FETCH_PAYMENT,
    FETCH_PAYMENT_FULFILLED, USER_PAY_AMOUNT,
} from './action-types';

export const updatePayment = (payment) => ({
    type: UPDATE_PAYMENT,
    payment,
});
export const fetchPayment = (orderIDs) => ({
    type: FETCH_PAYMENT,
    orderIDs,
});

export const fetchPaymentFulfilled = (payment) => ({
    type: FETCH_PAYMENT_FULFILLED,
    payment,
});

export const userPayAmount = (paymentPassword, orderIDs) => ({
    type: USER_PAY_AMOUNT,
    paymentPassword,
    orderIDs,
});