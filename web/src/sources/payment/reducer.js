/**
 * Created by isbdnt on 2017/12/27.
 */
import { UPDATE_PAYMENT, FETCH_PAYMENT_FULFILLED, } from './action-types';

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_PAYMENT_FULFILLED:
        case UPDATE_PAYMENT:
            return action.payment;
        default:
            return state;
    }
};