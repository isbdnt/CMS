/**
 * Created by isbdnt on 2017/12/24.
 */
import {
    FETCH_ITEMS, FETCH_ITEMS_FULFILLED,
} from './action-types';


export const fetchItems = (keyword, page) => ({
    type: FETCH_ITEMS,
    keyword,
    page,
});

export const fetchItemsFulfilled = (keyword, page, items) => ({
    type: FETCH_ITEMS_FULFILLED,
    keyword,
    page,
    items,
});