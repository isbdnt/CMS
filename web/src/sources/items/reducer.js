/**
 * Created by isbdnt on 2017/12/24.
 */
import {
    FETCH_ITEMS, FETCH_ITEMS_FULFILLED,
} from './action-types';
import _ from 'lodash';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_ITEMS:
            return state;
        case FETCH_ITEMS_FULFILLED:
            return {
                ...state,
                [action.keyword]: {
                    ...state[ action.keyword ],
                    [action.page]: action.items,
                },
            };
        default:
            return state;
    }
};