/**
 * Created by isbdnt on 2017/12/24.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/throttle';
import 'rxjs/add/operator/catch';
import { fetchItemsFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import { FETCH_ITEMS, } from './action-types';
import numericalValues from 'resources/configs/numerical-values.json';
import items from 'api/v1-0/items';
import { splitWords, getIntervalObservable } from 'utils';

const fetchItemsEpic = (action$, store) =>
    action$.ofType(FETCH_ITEMS)
        .throttle(() => getIntervalObservable())
        .mergeMap(({ keyword, page, }) =>
            items.getMany({
                offset: page * numericalValues.itemPageCount,
                count: numericalValues.itemPageCount,
                keywords: splitWords(keyword, store.getState().language.$type),
            })
                .map(({ data, }) => fetchItemsFulfilled(keyword, page, data)))
        .catch(e => console.error(e));

export default combineEpics(
    fetchItemsEpic
);