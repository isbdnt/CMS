/**
 * Created by isbdnt on 2017/12/16.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import searchAdvices from 'api/v1-0/search-advices';
import { FETCH_SEARCH_ADVICES, } from './action-types';
import { fetchSearchAdvicesFulfilled, } from './actions';
import { splitWords, } from 'utils';

const fetchSearchAdvicesEpic = (action$, store) =>
    action$.ofType(FETCH_SEARCH_ADVICES)
        .mergeMap(({ keyword, }) =>
            searchAdvices.getMany({
                keywords: splitWords(keyword, store.getState().language.$type),
            })
                .map(({ data, }) => fetchSearchAdvicesFulfilled(data)))
        .catch(e => console.error(e));


export default fetchSearchAdvicesEpic;