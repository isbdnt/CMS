/**
 * Created by isbdnt on 2017/12/16.
 */
import { FETCH_SEARCH_ADVICES_FULFILLED, FETCH_SEARCH_ADVICES, } from './action-types';


export default (state = [], action) => {
    switch (action.type) {
        case FETCH_SEARCH_ADVICES:
            return state;
        case FETCH_SEARCH_ADVICES_FULFILLED:
            return action.searchAdvices;
        default:
            return state;
    }
};