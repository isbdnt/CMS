/**
 * Created by isbdnt on 2017/12/16.
 */
import { FETCH_SEARCH_ADVICES, FETCH_SEARCH_ADVICES_FULFILLED, } from './action-types';

export const fetchSearchAdvices = (keyword) => ({
    type: FETCH_SEARCH_ADVICES,
    keyword,
});

export const fetchSearchAdvicesFulfilled = (searchAdvices) => ({
    type: FETCH_SEARCH_ADVICES_FULFILLED,
    searchAdvices,
});