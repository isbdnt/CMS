/**
 * Created by isbdnt on 2017/12/13.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { fetchUserFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import { FETCH_USER, } from './action-types';
import users from 'api/v1-0/users';

const fetchUserEpic = action$ =>
    action$.ofType(FETCH_USER)
        .mergeMap(({ concise, }) =>
            users.get({
                concise,
            })
                .map(({ data, }) => fetchUserFulfilled(data)))
        .catch(e => console.error(e));

export default combineEpics(fetchUserEpic);