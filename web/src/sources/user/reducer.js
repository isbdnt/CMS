/**
 * Created by isbdnt on 2017/12/13.
 */
import {
    FETCH_USER,
    FETCH_USER_FULFILLED,
} from './action-types';

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_USER:
            return state;
        case FETCH_USER_FULFILLED:
            return action.user;
        default:
            return state;
    }
};