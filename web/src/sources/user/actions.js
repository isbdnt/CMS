/**
 * Created by Administrator on 2017.12.15.
 */
import {
    FETCH_USER,
    FETCH_USER_FULFILLED,
} from './action-types';

export const fetchUser = () => ({
    type: FETCH_USER,
});

export const fetchUserFulfilled = user => ({
    type: FETCH_USER_FULFILLED,
    user,
});
