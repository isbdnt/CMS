/**
 * Created by isbdnt on 2017/12/24.
 */
import {
    FETCH_ITEM, FETCH_ITEM_FULFILLED,
    REPLACE_ITEM,
} from './action-types';

export const fetchItem = (itemID) => ({
    type: FETCH_ITEM,
    itemID,
});

export const fetchItemFulfilled = (item) => ({
    type: FETCH_ITEM_FULFILLED,
    item,
});

export const replaceItem = (item) => ({
    type: REPLACE_ITEM,
    item,
});