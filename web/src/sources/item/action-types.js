/**
 * Created by isbdnt on 2017/12/24.
 */
export const FETCH_ITEM = 'FETCH_ITEM';
export const FETCH_ITEM_FULFILLED = 'FETCH_ITEM_FULFILLED';
export const REPLACE_ITEM = 'REPLACE_ITEM';
