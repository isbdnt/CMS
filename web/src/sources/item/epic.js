/**
 * Created by isbdnt on 2017/12/24.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/throttle';
import 'rxjs/add/operator/catch';
import { getIntervalObservable, } from 'utils';
import { fetchItemFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import { FETCH_ITEM, } from './action-types';
import items from 'api/v1-0/items';

const fetchItemEpic = action$ =>
    action$.ofType(FETCH_ITEM)
        .throttle(() => getIntervalObservable())
        .mergeMap(({ itemID, }) =>
            items.get({
                itemID,
            })
                .map(({ data, }) => fetchItemFulfilled(data)))
        .catch(e => console.error(e));

export default combineEpics(
    fetchItemEpic
);