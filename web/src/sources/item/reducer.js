/**
 * Created by isbdnt on 2017/12/24.
 */
import {
    FETCH_ITEM, FETCH_ITEM_FULFILLED,
    REPLACE_ITEM,
} from './action-types';

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_ITEM:
            return state;
        case FETCH_ITEM_FULFILLED:
            return action.item;
        case REPLACE_ITEM:
            return action.item;
        default:
            return state;
    }
}