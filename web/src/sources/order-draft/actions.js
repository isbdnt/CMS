/**
 * Created by isbdnt on 2017/12/26.
 */
import {
    FETCH_ORDER_DRAFT, FETCH_ORDER_DRAFT_FULFILLED,
    CREATE_ORDER_DRAFT, CREATE_ORDER_DRAFT_FULFILLED,
    SUBMIT_ORDER_DRAFT,
} from './action-types';

export const fetchOrderDraft = (draftID) => ({
    type: FETCH_ORDER_DRAFT,
    draftID,
});

export const fetchOrderDraftFulfilled = (draft) => ({
    type: FETCH_ORDER_DRAFT_FULFILLED,
    draft,
});

export const createOrderDraft = (shoppingCartRecords) => ({
    type: CREATE_ORDER_DRAFT,
    shoppingCartRecords,
});

export const createOrderDraftFulfilled = (draft, recordIDs) => ({
    type: CREATE_ORDER_DRAFT_FULFILLED,
    draft,
    recordIDs,
});

export const submitOrderDraft = (draft) => ({
    type: SUBMIT_ORDER_DRAFT,
    draft,
});