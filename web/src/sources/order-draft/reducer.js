/**
 * Created by isbdnt on 2017/12/26.
 */
import {
    CREATE_ORDER_DRAFT, CREATE_ORDER_DRAFT_FULFILLED,
    FETCH_ORDER_DRAFT, FETCH_ORDER_DRAFT_FULFILLED,
} from './action-types';

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_ORDER_DRAFT_FULFILLED:
        case CREATE_ORDER_DRAFT_FULFILLED:
            return action.draft;
        case FETCH_ORDER_DRAFT:
        case CREATE_ORDER_DRAFT:
        default:
            return state;
    }
};