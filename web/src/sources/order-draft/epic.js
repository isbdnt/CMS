/**
 * Created by isbdnt on 2017/12/26.
 */
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/throttle';
import 'rxjs/add/operator/catch';
import { getIntervalObservable, } from 'utils';
import userDrafts from 'api/v1-0/users/order-drafts';
import drafts from 'api/v1-0/order-drafts';
import { CREATE_ORDER_DRAFT, FETCH_ORDER_DRAFT, SUBMIT_ORDER_DRAFT, } from './action-types';
import { createOrderDraftFulfilled, fetchOrderDraftFulfilled, } from './actions';
import { combineEpics, } from 'redux-observable';
import internalSites from 'resources/configs/internalSites.json';
import { createOrder, } from 'sources/order/actions';
import { createOrders, } from 'sources/orders/actions';
import { pushHistory, } from 'sources/query/actions';

const createOrderDraftEpic = (action$, store) =>
    action$.ofType(CREATE_ORDER_DRAFT)
        .mergeMap(({ shoppingCartRecords, }) =>
            userDrafts.post({
                userID: store.getState().user.id,
                shoppingCartRecords: shoppingCartRecords.map(({ item, count, }) => ({
                    itemID: item.id,
                    count,
                })),
            })
                .flatMap(({ data, }) => [
                    createOrderDraftFulfilled(data, shoppingCartRecords
                        .map(({ id, }) => id)
                        .filter(id => id)),
                    pushHistory(internalSites.orderDraft.$url, { draftID: data.id, }),
                ]))
        .catch(e => console.error(e));

const fetchOrderDraftEpic = (action$) =>
    action$.ofType(FETCH_ORDER_DRAFT)
        .throttle(() => getIntervalObservable())
        .mergeMap(({ draftID, }) =>
            drafts.get({
                draftID,
            })
                .map(({ data, }) => fetchOrderDraftFulfilled(data)))
        .catch(e => console.error(e));

const submitOrderDraftEpic = (action$) =>
    action$.ofType(SUBMIT_ORDER_DRAFT)
        .map(({ draft, }) => {
            if (draft.purchaseRecords.length > 1) {
                return createOrders(draft.purchaseRecords.map(({ item, count, }) => ({
                    itemID: item.id,
                    count,
                    addressID: draft.address.id,
                })));
            }
            else {
                const { item, count, } = draft.purchaseRecords[ 0 ];
                return createOrder(item.id, count, draft.address.id);
            }
        })
        .catch(e => console.error(e));

export default combineEpics(
    createOrderDraftEpic,
    fetchOrderDraftEpic,
    submitOrderDraftEpic
);