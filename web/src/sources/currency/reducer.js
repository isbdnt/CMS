/**
 * Created by isbdnt on 2017/12/25.
 */
import { UPDATE_CURRENCY, } from './action-types';

export default (state = window.localStorage.getItem('currency') || 'cny', action) => {
    switch (action.type) {
        case UPDATE_CURRENCY:
            window.localStorage.setItem('currency', action.currency);
            return action.currency;
        default:
            return state;
    }
};