/**
 * Created by isbdnt on 2017/12/25.
 */
import { UPDATE_CURRENCY, } from './action-types';

export const updateCurrency = (currency) => ({
    type: UPDATE_CURRENCY,
    currency,
});