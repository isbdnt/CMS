const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
    entry: {
        app: [
            'react-hot-loader/patch',
            // activate HMR for React
            'webpack-dev-server/client?http://localhost:3000',
            // bundle the client for webpack-dev-server
            // and connect to the provided endpoint
            'webpack/hot/only-dev-server',
            // bundle the client for hot reloading
            // only- means to only hot reload for successful updates
            './index.js',
            // the entry point of our app
        ],
    },
    devtool: 'source-map',
    devServer: {
        port: 3000,
        contentBase: './dev',
        stats: 'minimal',
        hot: true,
        historyApiFallback: true,
        publicPath: '/',
    },
    plugins: [
        new CleanWebpackPlugin([ 'dev', ]),
        new HtmlWebpackPlugin({
            template: './resources/index.dev.html',
            filename: 'index.html',
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dev'),
        publicPath: '/',
    },
});