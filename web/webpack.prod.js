const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
        'react-redux': 'ReactRedux',
        'redux': 'Redux',
        'rxjs': 'Rx',
        'lodash': '_',
        'qs': 'Qs',
        'prop-types': 'PropTypes',
        // 'redux-observable': 'asd',
        // jQuery: 'jQuery',
        // 'react-router': 'ReactRouter',
        // 'react-router-redux': 'ReactRouterRedux',
        // // 'react-hyperscript': 'ReactHyperscript',
        'react-router-dom': 'ReactRouterDOM',
    },
    entry: {
        'app': './index.js',
    },
    module: {
        rules: [
            {
                test: /babel-polyfill/,
                use: 'ignore-loader',
            },
        ],
    },
    devtool: 'source-map',
    devServer: {
        port: 5000,
        contentBase: './bin',
        stats: 'minimal',
        historyApiFallback: true,
        publicPath: '/',
    },
    plugins: [
        new CleanWebpackPlugin([ 'bin', ]),
        new HtmlWebpackPlugin({
            template: './resources/index.prod.html',
            filename: 'index.html',
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
        }),
        new CompressionPlugin({
            test: /\.jsx?$/,
            asset: '[file].gz',
        }),
    ],
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, 'bin'),
        publicPath: '/',
    },
});