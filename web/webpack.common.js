const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    context: path.join(__dirname, 'src/'),
    resolve: {
        modules: [ 'src', 'node_modules', ],
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
                test: /\.(png|jpg)$/,
                exclude: /node_modules/,
                use: 'url-loader?limit=8192',
            },
        ],
    },
    plugins: [
        new CopyPlugin([
            { from: './resources/favicon.ico', to: 'favicon.ico', },
        ]),
    ],
};